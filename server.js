const express = require('express')
const app = express()
const fs = require('fs')
const request = require('request')
let port = process.env.PORT || 3000

app.use(express.static(__dirname + '/public'))
app.set('view engine', 'ejs')
app.set('views', __dirname)

app.get('/', (req, res) => {
    return new Promise((resolve, reject) => {
        let randomNum = Math.floor(Math.random() * 7) + 0;
        resolve(randomNum)
    })
    .then(num => {
        let dynamicClock = ''
        switch(parseInt(num + 1)) {
            case 1:
                dynamicClock = 'Eu sou um relógio digital'
            break;
            case 2:
                dynamicClock = 'Eu sou um relógio de ouro'
            break;
            case 3:
                dynamicClock = 'Eu sou um relógio de pêndulo'
            break;
            case 4:
                dynamicClock = 'Eu sou um relógio despertador'
            break; 
            case 5:
                dynamicClock = 'Eu sou um cronômetro digital'
            break;
            case 6:
                dynamicClock = 'Eu sou um relógio preciso'
            break;
            case 7:
                dynamicClock = 'Eu sou um relógio clássico'
            break;
        }
        
        res.render('index', {dynamicImg: 'http://doadordeorgaos.com/' + parseInt(num + 1)  + '.jpg', dynamicClock: dynamicClock})

        request.post('https://graph.facebook.com/v2.10/?scrape=true&id=http://doadordeorgaos.com/', {
            json: {
                "access_token":"EAABqrvZA91h8BANrTRQsQDKLEI9C5yQIyOm2ZCwhvmmBi0gRZABMfyzolr4RMsmeNzz5iwB61bUtQwadKZB9IiPgncBUMrfjzQqmaMRzJboTNhTICUxS6QpDFC6zZCAARbBOm7UW7YL3agAZB6ZCnP5EaWE57xuPmF7axWgG1TH1QZDZD"
            }
        })
    })
})

setInterval(() => {
    request.get('http://doadordeorgaos.com/') 
}, 12000)

app.listen(port, () => console.log('running at ' + port)) 